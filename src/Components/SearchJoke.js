import React, { useState } from 'react';
import useAxios from '../CustomHooks/useAxios';
import { BASE_URL, LOADING_ICON } from '../Constants';

const SearchJoke = () => {

    const res = useAxios(BASE_URL);
    const { data, isError, isLoading, axiosCallback } = res;
    const [jokeList, setJokeList] = useState([]);

    const callApi = (e) => {
        axiosCallback(BASE_URL + 'search?term=' + e.target.value);
        setJokeList(data);
        console.log(jokeList);
    }

    return (
        <React.Fragment>
            <h1>Search for a joke</h1>
            <input type="text" placeholder="Joke Keyword" onChange={callApi}></input>
            {isError && <div>Something went wrong ...</div>}
            {jokeList.results &&
                <React.Fragment>
                {isLoading ? (
                    <img src={LOADING_ICON} alt="Loading Icon" />
                ) : (
                <ul>
                    {jokeList.results.map(item => (
                        <li key="item.id">{item.joke}</li>
                    ))}
                </ul>
                )}
                </React.Fragment>
            }
        </React.Fragment>
    );
};

export default SearchJoke;