import React from 'react';
import useAxios from '../CustomHooks/useAxios';
import { BASE_URL, LOADING_ICON } from '../Constants';

const GetJoke = () => {

    const res = useAxios(BASE_URL);
    const { data, isError, isLoading, axiosCallback } = res;

    return (
        <React.Fragment>
            <h1>Get Joke</h1>
            {isError && <div>Something went wrong ...</div>}
            {isLoading ? (
                <img src={LOADING_ICON} alt="Loading Icon" />
            ) : (
                <p>{data.joke}</p>
            )}
            <button onClick={() => axiosCallback(BASE_URL)}>Get new joke</button>
        </React.Fragment>
    );
};

export default GetJoke;