import React from 'react';
import GetJoke from './Components/GetJoke';
import SearchJoke from './Components/SearchJoke';
import './App.css';

function App() {
  return (
    <div className="App">
      <GetJoke />
      <hr />
      <SearchJoke />
    </div>
  );
}

export default App;
