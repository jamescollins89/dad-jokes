import { useEffect, useState } from 'react';
import axios from 'axios';

const useAxios = (url) => {
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [isError, setIsError] = useState(false);

    const fetchData = async (url) => {
        setIsError(false);
        setIsLoading(true);
        try {
            const result = await axios({
                method: 'get',
                url,
                headers: { "Accept": "application/json" }
            });
            setData(result.data);
        } catch (error) {
            setIsError(true);
        }
        setIsLoading(false);
    };

    useEffect(() => {
        fetchData(url);
    }, [url]);

    const axiosCallback = (url) => {
        fetchData(url);
        console.log(url);
    }

    return {isLoading, data, isError, axiosCallback};

    // const axiosCallback = useCallback(() => {
    //     fetchData();
    // }, [url])

}

export default useAxios;