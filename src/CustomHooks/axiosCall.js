import { useEffect, useState } from 'react';
import axios from 'axios';

const axiosCall = (url) => {
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [isError, setIsError] = useState(false);
    const [axiosSettings, setAxiosSettings] = useState({
        method: 'get',
        url: url,
        headers: { "Accept": "application/json" }
    });

    const fetchData = async () => {
        setIsError(false);
        setIsLoading(true);
        try {
            const result = await axios(axiosSettings);
            setData(result.data);
        } catch (error) {
            setIsError(true);
        }
        setIsLoading(false);
    };

    useEffect(() => {
        fetchData();
    }, [url]);
    return {isLoading, data, isError};
}

export default axiosCall;